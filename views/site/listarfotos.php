<?php
use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'layout' =>"{items}",
    'columns' =>[
        [
            'label'=>'Fotos',
            'format'=>'raw',
            'value'=>function($data){
                $url='@web/imgs/' . $data->nombre;
                return Html::img($url,['class'=>'img-fluid',"width"=>300,"height"=>200]);
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{eliminar}',
            'buttons' => [
                'eliminar' => function ($url,$model) {
                    return Html::a('<i class="far fa-trash-alt"></i>',
                            ['site/eliminarfoto',"codigo"=>$model->codigo],
                            ['data' => ['confirm' => '¿Estas seguro que deseas eliminar la foto seleccionada?',
                                        'method' => 'post']]);
                },
                        ]
        ]
    ]
    ]);

?>