<?php
use yii\grid\GridView;
use yii\helpers\Html;

?>
<h1 class="border rounded bg-secondary p-3 text-white text-center mb-5"><?= $noticia->titulo ?></h1>
<h4><?= $noticia->texto ?></h4>
<br>
Comentarios de la noticia
<?php

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' =>"{items}",
    'columns' =>[
        "texto",
        "fecha",
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{editar} {eliminar}',
            'buttons' => [
                'editar' => function ($url,$model) {
                    return Html::a('<i class="fas fa-pencil-alt"></i>', 
                       ['site/editarcomentario',"codigo"=>$model->codigo]);
                },
                'eliminar' => function ($url,$model) {
                    return Html::a('<i class="far fa-trash-alt"></i>',
                            ['site/eliminarcomentario',"codigo"=>$model->codigo],
                            ['data' => ['confirm' => '¿Estas seguro que deseas borrar el comentario?',
                                        'method' => 'post']]);
                },
                        ]
        ]
    ]
    ]);

?>

