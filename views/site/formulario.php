<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Formulario */
/* @var $form ActiveForm */
?>
<div class="formulario">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'texto')->textarea() ?>
    
        <div class="form-group">
            <?= Html::submitButton('Añadir Comentario', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- formulario -->
