<?php
use yii\widgets\ListView;
use yii\helpers\Html;

?>
<h1 class="border rounded bg-secondary p-3 text-white text-center mb-5">Noticias</h1>

<?= Html::a("Nueva Noticia",["site/nuevanoticia"], ['class' => 'btn btn-primary float-right']) ?>
<br><br>

<?php

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_noticias',
    "itemOptions" => [
        'class' => 'col-lg-4 ml-auto mr-auto bg-light p-3 mb-5',
    ],
    "options" => [
        'class' => 'row',
    ],
    'layout'=>"{items}"

    ]);

?>

