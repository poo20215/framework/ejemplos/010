<?php
use yii\helpers\Html;
use yii\bootstrap4\Carousel;
?>

<div class="row">
    <div class="col-lg-12">
        <h2 style="color: blue;"><?= $model->titulo ?></h2><br>
        <div class="bg-warning rounded p-2">Fecha:</div>
        <div class="p-1"><?= $model->fecha ?></div>
        <div class="bg-warning rounded p-2">Contenido:</div>
        <div class="p-1 mb-4"><?= $model->texto ?></div>
    </div>
</div>

<?php
$carousel=$model->fotosCarusel($model->codigo);
if(count($carousel)==0)
{
    echo Html::img("@web/imgs/imagen1.jpg",["width"=>200,"height"=>200,"style"=>"margin-left:55px;"]);
    echo "<br>";
}else
{
echo Carousel::widget([
    'items' => $carousel,
    'options'=>[
        'class' => 'mx-auto col-lg-8'
    ],
    'controls' => ['<i class="fas fa-arrow-left fa-3x"></i>','<i class="fas fa-arrow-right fa-3x"></i>']
]);
}
?>
<br>
<?= Html::a("Añadir Comentario",["site/anadircomentario", 'codigo' => $model->codigo], ['class' => 'btn btn-primary']) ?>
&nbsp
<?= Html::a("Ver Comentarios",["site/vercomentarios", 'codigo' => $model->codigo], ['class' => 'btn btn-primary']) ?>
<br><br>
<?= Html::a("Editar Noticia",["site/editarnoticia", 'codigo' => $model->codigo]) ?>
&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
<?= Html::a("Eliminar Noticia",["site/confirmareliminarnoticia", 'codigo' => $model->codigo]) ?>
