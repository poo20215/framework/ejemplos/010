<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => 'Noticias', 'url' => ['/site/index']],
            ['label' => 'Fotos', 'items' =>[
                ['label' => 'Añadir Fotos', 'url' => ['/site/anadirfotos']],
                ['label' => 'Eliminar Fotos', 'url' => ['/site/listarfotos']]]],
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container" style="color: black">
        <div class="row">
            <div class="col-md-4"><h4>DATOS DE CONTACTO</h4>
                <ul style="list-style:none">
                    <li><h6><b>Diego`s Company</b></h6></li>
                    <li><h6><b>942784653</b></h6></li>
                    <li><h6><b>diego@gmail.com</b></h6></li>
                </ul>
            </div>
            <div class="col-md-4"><h4>REDES SOCIALES</h4>
                    <a href="https://www.facebook.com/MarketingTCM/" style="float:left;"><h1><i class="fab fa-facebook-square"></i>&nbsp;</h1></a>
                    <a href="https://www.instagram.com/marketing_tcm/" style="float:left;"><h1><i class="fab fa-instagram-square"></i>&nbsp;</h1></a>
                    <a href="https://www.youtube.com/channel/UClERLxGKsbQqKoX7-Y_IMGw" ><h1><i class="fab fa-youtube-square"></i></h1>&nbsp;</a>
            </div>
            <div class="col-md-4"><h4>DESCARGAR APP</h4>
                <a href="https://play.google.com/store/apps/details?id=com.marketingtcm.marketingtcm"><h1 style="float:left;"><i class="fab fa-google-play"></i>&nbsp;</h1><h2>Google Play</h2></a>
            </div>
        </div>
        <p class="float-left">My Company</p>
        <p class="float-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
