-- MySQL dump 10.13  Distrib 5.7.36, for Win64 (x86_64)
--
-- Host: localhost    Database: ejemplo10yii
-- ------------------------------------------------------
-- Server version	5.7.36-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comentarios`
--

DROP TABLE IF EXISTS `comentarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comentarios` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `texto` text,
  `fecha` date DEFAULT NULL,
  `noticias_codigo` int(11) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `fk_comentarios_noticias` (`noticias_codigo`),
  CONSTRAINT `fk_comentarios_noticias` FOREIGN KEY (`noticias_codigo`) REFERENCES `noticias` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentarios`
--

LOCK TABLES `comentarios` WRITE;
/*!40000 ALTER TABLE `comentarios` DISABLE KEYS */;
INSERT INTO `comentarios` VALUES (1,'comentario para la noticia1','2021-11-11',1),(2,'comentario2 noticia1','2021-11-10',1),(3,'comenatriosnoticia2','2021-11-11',2),(4,'probando comentario noticia2',NULL,2),(5,'probando comentario noticia1',NULL,1),(6,'probando comentario noticia1',NULL,1),(7,'probando comentario noticia2 2',NULL,2),(8,'Que noticia mas interesante',NULL,1),(9,'Que noticia mas interesante 2',NULL,1),(10,'añadiendo otro comentario',NULL,2);
/*!40000 ALTER TABLE `comentarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fotos`
--

DROP TABLE IF EXISTS `fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fotos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fotos`
--

LOCK TABLES `fotos` WRITE;
/*!40000 ALTER TABLE `fotos` DISABLE KEYS */;
INSERT INTO `fotos` VALUES (1,'imagen1.jpg'),(2,'imagen2.jpg'),(3,'imagen3.jpg'),(4,'imagen4.jpg'),(5,'imagen5.jpg'),(6,'imagen6.jpg'),(7,'imagen7.jpg');
/*!40000 ALTER TABLE `fotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticias`
--

DROP TABLE IF EXISTS `noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticias` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) DEFAULT NULL,
  `texto` text,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticias`
--

LOCK TABLES `noticias` WRITE;
/*!40000 ALTER TABLE `noticias` DISABLE KEYS */;
INSERT INTO `noticias` VALUES (1,'Noticia1','prueba noticia 1','2021-11-10'),(2,'Noticia2','prueba noticia 2','2021-11-08'),(3,'Noticia3','Prueba noticia3','2021-11-12'),(4,'Noticia4','Prueba noticia4','2021-11-09'),(5,'Noticia5','Prueba noticia5','2021-11-11');
/*!40000 ALTER TABLE `noticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticias_fotos`
--

DROP TABLE IF EXISTS `noticias_fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticias_fotos` (
  `noticias_codigo` int(11) NOT NULL,
  `fotos_codigo` int(11) NOT NULL,
  `visitas` int(11) DEFAULT '0',
  PRIMARY KEY (`noticias_codigo`,`fotos_codigo`),
  KEY `fk_noticiasfotos_fotos` (`fotos_codigo`),
  CONSTRAINT `fk_noticiasfotos_fotos` FOREIGN KEY (`fotos_codigo`) REFERENCES `fotos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_noticiasfotos_noticias` FOREIGN KEY (`noticias_codigo`) REFERENCES `noticias` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticias_fotos`
--

LOCK TABLES `noticias_fotos` WRITE;
/*!40000 ALTER TABLE `noticias_fotos` DISABLE KEYS */;
INSERT INTO `noticias_fotos` VALUES (1,2,0),(1,4,0),(1,5,0),(2,2,0),(2,6,0),(3,1,0),(3,7,0),(4,2,0),(4,4,0),(4,6,0),(5,3,0),(5,5,0),(5,7,0);
/*!40000 ALTER TABLE `noticias_fotos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-12 12:01:34
