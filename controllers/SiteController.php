<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Noticias;
use yii\data\ActiveDataProvider;
use app\models\Comentarios;
use app\models\NoticiasFotos;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\SubirFotosForm;
use app\models\Fotos;
use yii\web\UploadedFile;
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Noticias::find()]);
        return $this->render("index",["dataProvider" => $dataProvider]);
         
    }
    
    public function actionAnadircomentario($codigo)
    {
        $model = new Comentarios();

        if ($model->load(Yii::$app->request->post())) {
            $model->noticias_codigo=$codigo;
            $fecha=date('Y-m-d');
            $model->fecha=$fecha;
            if ($model->save()) 
            {
                return $this->redirect(["site/vercomentarios","codigo"=>$codigo]);
            }
        }

        return $this->render('formulario', [
            'model' => $model,
        ]);
    
    }
    
    public function actionVercomentarios($codigo)
    {
        $noticia= Noticias::findOne($codigo);
        $dataProvider = new ActiveDataProvider([
        'query' => Comentarios::find()->where(['noticias_codigo' => $codigo])]);
        return $this->render("listar",["dataProvider" => $dataProvider,"noticia" => $noticia]);
    }
    
    public function actionEditarcomentario($codigo)
    {
        $model=Comentarios::findOne($codigo);
        
        if($this->request->isPost)
        {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) 
                {
                    return $this->redirect(["site/vercomentarios","codigo"=>$model->noticias_codigo]);
                }
            }
        }
        
        return $this->render('formulario', [
            'model' => $model,
        ]);
                
    }
    
    public function actionEliminarcomentario($codigo)
    {
        $model=Comentarios::findOne($codigo);
        $noticias_codigo=$model->noticias_codigo;
        $model->delete();
        return $this->redirect(["site/vercomentarios","codigo"=>$noticias_codigo]);
    }
    
    public function actionNuevanoticia()
    {
        $model=new Noticias();
        
        
        if($datos=Yii::$app->request->post())
        {
            $model->titulo=$datos["titulo"];
            $model->texto=$datos["texto"];
            $model->fecha=$datos["fecha"];
            $model->save();
            $ultima_noticia= Noticias::find("codigo")->orderBy("codigo DESC")->one();
            foreach($datos["fotos"] as $foto)
            {
                $model_2=new NoticiasFotos();
                $model_2->noticias_codigo=$ultima_noticia->codigo;
                $model_2->fotos_codigo=$foto;
                $model_2->save();
            }
            return $this->redirect(["site/index"]);
            
        }else
        {
            return $this->render("formulario_noticias2");
        }
    }
    
    public function actionFormulario1()
    {
        if($datos=Yii::$app->request->post())
        {
            var_dump($datos);
            
        }else
        {
            return $this->render("formulario_noticias2");
        }
    }
    
    public function actionEditarnoticia($codigo)
    {
        $model= Noticias::findOne($codigo);
        
        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->validate()) {
                $model->save();
                return $this->redirect(["site/index"]);
                }
        }

        return $this->render('formulario_noticias', [
            'model' => $model,
        ]);
    }
    
    public function actionEliminarnoticia($codigo)
    {
        $model=Noticias::findOne($codigo);
        $model->delete();
        return $this->redirect(["site/index"]);
    }
    
    public function actionConfirmareliminarnoticia($codigo)
    {
        $model=Noticias::findOne($codigo);
        
        return $this->render("ver",['model'=> $model]);
    }
    
    public function actionAnadirfotos()
    {
        $model=new SubirFotosForm();
        $mensaje=null;
        $imagen_repetida=null;
        
        if ($model->load(Yii::$app->request->post()))
    {
     $model->imagenes = UploadedFile::getInstances($model, 'imagenes');

     if ($model->imagenes && $model->validate()) 
     {
         $imagenes_bd= Fotos::find()->asArray()->all();
         $imagenes_bd= ArrayHelper::getColumn($imagenes_bd,"nombre");
        foreach ($model->imagenes as $imagen) 
        {   
            foreach($imagenes_bd as $imagen_bd)
            {
                if($imagen->name==$imagen_bd)
                {
                    $imagen_repetida=$imagen->name;
                    break;
                }
            }

            if($imagen_repetida==null)
            {
                $imagen->saveAs('imgs/' . $imagen->name);
                $mensaje = "<p><strong class='label label-info'>Enhorabuena, subida realizada con éxito</strong></p>";
                $foto=new Fotos;
                $foto->nombre=$imagen->name;
                $foto->save();
            }else
            {
                $mensaje = "<p><strong class='label label-info'>Error, ya existe una imagen llamada $imagen_repetida</strong></p>";
                break;
            }

        }
     }
    }
    return $this->render("formulario_fotos", ["model" => $model, "mensaje" => $mensaje]);
    }
    
    public function actionListarfotos()
    {
         $dataProvider = new ActiveDataProvider([
        'query' => Fotos::find(),
             'pagination' => ['pageSize' => 6]
             ]);
        return $this->render("listarfotos",["dataProvider" => $dataProvider]);
    }
    
    public function actionEliminarfoto($codigo)
    {
        $imagen= Fotos::findOne($codigo);
        unlink("../web/imgs/" . $imagen->nombre);
        $imagen->delete();
        return $this->redirect(["site/listarfotos"]);
    }

}
