<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "noticias".
 *
 * @property int $codigo
 * @property string|null $titulo
 * @property string|null $texto
 * @property string|null $fecha
 *
 * @property Comentarios[] $comentarios
 * @property Fotos[] $fotosCodigos
 * @property NoticiasFotos[] $noticiasFotos
 */
class Noticias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['texto'], 'string'],
            [['fecha'], 'safe'],
            [['titulo'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'titulo' => 'Titulo',
            'texto' => 'Texto',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasMany(Comentarios::className(), ['noticias_codigo' => 'codigo']);
    }

    /**
     * Gets query for [[FotosCodigos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFotosCodigos()
    {
        return $this->hasMany(Fotos::className(), ['codigo' => 'fotos_codigo'])->viaTable('noticias_fotos', ['noticias_codigo' => 'codigo']);
    }

    /**
     * Gets query for [[NoticiasFotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasFotos()
    {
        return $this->hasMany(NoticiasFotos::className(), ['noticias_codigo' => 'codigo']);
    }
    
    public function fotosCarusel($codigo)
    {
        $salida=[];
        $fotos=Yii::$app->db->createCommand("select nombre from noticias_fotos inner join fotos on fotos_codigo=codigo where noticias_codigo=$codigo")->queryAll();
        $fotos= ArrayHelper::getColumn($fotos,"nombre");
        foreach($fotos as $src)
        {
            $salida[]= Html::img("@web/imgs/$src",["width"=>300,"height"=>200]);
        }
        return $salida;
    }
}
