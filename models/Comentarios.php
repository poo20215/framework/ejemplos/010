<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comentarios".
 *
 * @property int $codigo
 * @property string|null $texto
 * @property string|null $fecha
 * @property int|null $noticias_codigo
 *
 * @property Noticias $noticiasCodigo
 */
class Comentarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comentarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['texto'], 'string'],
            [['fecha'], 'safe'],
            [['noticias_codigo'], 'integer'],
            [['noticias_codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Noticias::className(), 'targetAttribute' => ['noticias_codigo' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'texto' => 'Texto',
            'fecha' => 'Fecha',
            'noticias_codigo' => 'Noticias Codigo',
        ];
    }

    /**
     * Gets query for [[NoticiasCodigo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasCodigo()
    {
        return $this->hasOne(Noticias::className(), ['codigo' => 'noticias_codigo']);
    }
}
