<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fotos".
 *
 * @property int $codigo
 * @property string|null $nombre
 *
 * @property Noticias[] $noticiasCodigos
 * @property NoticiasFotos[] $noticiasFotos
 */
class Fotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fotos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 50],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[NoticiasCodigos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasCodigos()
    {
        return $this->hasMany(Noticias::className(), ['codigo' => 'noticias_codigo'])->viaTable('noticias_fotos', ['fotos_codigo' => 'codigo']);
    }

    /**
     * Gets query for [[NoticiasFotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasFotos()
    {
        return $this->hasMany(NoticiasFotos::className(), ['fotos_codigo' => 'codigo']);
    }
}
