<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticias_fotos".
 *
 * @property int $noticias_codigo
 * @property int $fotos_codigo
 * @property int|null $visitas
 *
 * @property Fotos $fotosCodigo
 * @property Noticias $noticiasCodigo
 */
class NoticiasFotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias_fotos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['noticias_codigo', 'fotos_codigo'], 'required'],
            [['noticias_codigo', 'fotos_codigo', 'visitas'], 'integer'],
            [['noticias_codigo', 'fotos_codigo'], 'unique', 'targetAttribute' => ['noticias_codigo', 'fotos_codigo']],
            [['fotos_codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Fotos::className(), 'targetAttribute' => ['fotos_codigo' => 'codigo']],
            [['noticias_codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Noticias::className(), 'targetAttribute' => ['noticias_codigo' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'noticias_codigo' => 'Noticias Codigo',
            'fotos_codigo' => 'Fotos Codigo',
            'visitas' => 'Visitas',
        ];
    }

    /**
     * Gets query for [[FotosCodigo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFotosCodigo()
    {
        return $this->hasOne(Fotos::className(), ['codigo' => 'fotos_codigo']);
    }

    /**
     * Gets query for [[NoticiasCodigo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasCodigo()
    {
        return $this->hasOne(Noticias::className(), ['codigo' => 'noticias_codigo']);
    }
}
